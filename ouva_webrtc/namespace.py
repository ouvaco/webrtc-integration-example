"""
Custom socketio namespace
"""
import asyncio
import logging
import os
import random
import time
from datetime import datetime
from typing import Dict, Set, Optional

import socketio
from aioice import Candidate
from aiortc import RTCSessionDescription, RTCPeerConnection, \
	RTCIceServer, RTCConfiguration, MediaStreamTrack
from aiortc.rtcicetransport import candidate_from_aioice

from ouva_webrtc import SIO
from ouva_webrtc.media import ResizedMediaRecorder

logger = logging.getLogger("ouva")


class WebRTCCameraNamespace(socketio.AsyncClientNamespace):
	"""
	Custom namespace for the WebRTC client to handle custom messages
	"""

	def __init__(
			self, namespace: str = None,
			camera_id: str = None,
			width: int = 0,
			height: int = 0,
	):
		super().__init__(namespace)

		self.camera_id: str = camera_id

		self.width = width
		self.height = height
		self.session_reference_key = "webrtc"

		self.node_identity: str = ""
		self.login_success: bool = False
		self.peer_connections: Set = set()

		current_datetime = datetime.now()
		formatted_datetime = current_datetime.strftime("%Y-%m-%d_%H-%M-%S")
		self.video_name = f"recording/{formatted_datetime}_{self.camera_id}.mkv"
		os.makedirs("recording", exist_ok=True)

		self.recorder: Optional[ResizedMediaRecorder] = None

		self.ice_servers: RTCIceServer = RTCIceServer(urls='stun:stun.l.google.com:19302')
		self.peer_connection: RTCPeerConnection = RTCPeerConnection(
			configuration=RTCConfiguration(iceServers=[self.ice_servers]))

		self.exitcode = 0

	async def trigger_event(self, event: str, *args):
		"""
		Dispatch an event to the proper handler method.

		In the most common usage, this method is not overloaded by subclasses,
		as it performs the routing of events to methods. However, this
		method can be overridden if special dispatching rules are needed, or if
		having a single method that catches all events is desired.

		overridden because client messages contains space character and python-socketio doesn't handle it by default
		"""
		event = event.replace(" ", "_")  # this different from base function
		handler_name = 'on_' + event
		if hasattr(self, handler_name):
			return await getattr(self, handler_name)(*args)
		else:
			# append a generic function, so it doesn't break on every unknown message
			logger.info(
				"session %s: no special handler. calling generic for event %s",
				self.session_reference_key, event
			)
			return self.on_message(*args)

	async def on_connected_pod_name(self, data: str) -> None:
		logger.info("session %s: connected pod name: %s", self.session_reference_key, data)

	async def on_client_id(self, data: str) -> None:
		logger.info("session %s: client id: %s", self.session_reference_key, data)

	async def on_connect(self) -> None:
		logger.info("session %s: connection established", self.session_reference_key)
		await SIO.emit("get node identity")

	async def on_connect_error(self, data: str) -> None:
		logger.error("session %s: connection error: %s", self.session_reference_key, data)
		await SIO.disconnect()
		self.exitcode = -1

	async def on_controller_version(self, data: str) -> None:
		out_data = data.replace('\n', '')
		logger.info("session %s: controller version: %s", self.session_reference_key, out_data)

	async def on_node_online(self, camera_id: str, data: Dict) -> None:
		logger.info("session %s: node is online %s: %s", self.session_reference_key, camera_id, data)

	async def on_node_identity(self, data: str):
		logger.info("session %s: node identity: %s", self.session_reference_key, data)
		self.node_identity = data
		await SIO.emit_multiple(
			[
				"login node",
				self.node_identity,
				{'callType': "none", 'mediaRelayGroup': "default", 'mediaType': "none"},
				[],
				[]
			]
		)

	async def on_online_nodes(self, data: Dict) -> None:
		logger.info("session %s: online nodes: %s", self.session_reference_key, data)
		online_node_ids = [node['nodeId'] for node in data]

		if self.camera_id not in online_node_ids:
			logger.warning("session %s: camera is not reported as online", self.session_reference_key)

		user_id = str(int(time.time() * 1000)) + str(random.randint(0, 30000))

		await SIO.emit_multiple(["link user endpoint", self.camera_id, self.camera_id, user_id])

		await SIO.emit("login user", user_id, callback=self.on_login_user_callback)

	async def on_login_user_callback(self, data: str) -> None:
		login_result = data == "yes"
		if login_result:
			logger.info("session %s: login successful", self.session_reference_key)
			self.login_success = True
		else:
			logger.error("session %s: login unsuccessful. exiting...", self.session_reference_key)
			await SIO.disconnect()
			self.exitcode = -1
			raise ValueError()

	async def on_node_login_success(self, data: str) -> None:
		logger.info("session %s: node login success: %s", self.session_reference_key, data)
		self.login_success = True

	async def on_add_source(self, data1: str, data2: str) -> None:
		"""
		not sure what to do here. Client haven't explained this part. Probably legacy code
		:param data1:
		:param data2:
		:return:
		"""
		logger.info("session %s: add source: %s, %s", self.session_reference_key, data1, data2)

	async def on_offer(self, data1, params) -> None:
		logger.info("new offer for camera %s", data1)

		new_offer = RTCSessionDescription(sdp=params["sdp"], type=params["type"])

		self.peer_connections.add(self.peer_connection)

		# prepare local media
		self.recorder = ResizedMediaRecorder(self.video_name, width=self.width, height=self.height)

		@self.peer_connection.on("connectionstatechange")
		async def on_on_connectionstatechange():
			logger.info(
				"session %s: Connection state is %s", self.session_reference_key,
				self.peer_connection.connectionState
			)
			if self.peer_connection.connectionState == "failed":
				logger.warning("session %s: peer connection failed", self.session_reference_key)
				await self.peer_connection.close()
				await SIO.disconnect()
				self.peer_connections.discard(self.peer_connection)
				self.exitcode = -1
			elif self.peer_connection.connectionState == "close":
				logger.warning("session %s: peer connection closed", self.session_reference_key)
				await self.peer_connection.close()
				await SIO.disconnect()
				self.peer_connections.discard(self.peer_connection)
				if self.exitcode == 0:
					self.exitcode = -1
			elif self.peer_connection.connectionState == "connected":
				await SIO.emit_multiple(["peer ice state", self.camera_id, "connected", "stable"])

		@self.peer_connection.on("track")
		def on_track(track: MediaStreamTrack):
			logger.debug(track)
			logger.info("Track %s received", track.kind)

			if track.kind == "audio":
				self.recorder.addTrack(track)
			if track.kind == "video":
				logger.info("setting handler for video track")
				self.recorder.addTrack(track)

			@track.on("ended")
			async def on_on_ended():
				logger.info("Track %s ended", track.kind)
				await asyncio.gather(SIO.disconnect(), self.recorder.stop())

		# handle offer
		await self.peer_connection.setRemoteDescription(new_offer)
		await self.recorder.start()
		logger.info("remote description is sent and recorder is started")

		# send answer
		answer = await self.peer_connection.createAnswer()
		await self.peer_connection.setLocalDescription(answer)
		logger.info("the answer is created and local descriptor is set")

		# now sure why we are duplicating these events. But the client has implemented it this way
		await asyncio.gather(
			SIO.emit_multiple(["peer signaling state", self.camera_id, "have-remote-offer"]),
			SIO.emit_multiple(["receiving media", self.camera_id, self.camera_id, "audioVideo"]),
			SIO.emit_multiple(["receiving media", self.camera_id, self.camera_id, "audioVideo"]),
			SIO.emit_multiple(["peer signaling state", self.camera_id, "stable"]),
			SIO.emit_multiple(
				[
					"send", self.camera_id, "answer",
					{
						"type": self.peer_connection.localDescription.type,
						"sdp": self.peer_connection.localDescription.sdp
					}
				]
			)
		)

	async def on_ice(self, _, message) -> None:
		ice_message = message['candidate'].split(":")[1]
		logger.info("session %s: incoming ice message: %s", self.session_reference_key, ice_message)

		candidate = candidate_from_aioice(Candidate.from_sdp(ice_message))
		candidate.sdpMid = message['sdpMid']
		candidate.sdpMLineIndex = message['sdpMLineIndex']

		await self.peer_connection.addIceCandidate(candidate)

	async def on_message(self, *args, **kwargs) -> None:
		logger.warning("session %s: unhandled message: %s, %s", self.session_reference_key, args, kwargs)

	async def on_disconnect(self) -> None:
		logger.info('session %s: disconnected from server', self.session_reference_key)
		if self.recorder is not None:
			await self.recorder.stop()
		await SIO.disconnect()
		if self.exitcode == 0:
			self.exitcode = -1

	async def stop(self):
		if self.recorder is not None:
			await self.recorder.stop()
		if self.peer_connection is not None:
			await self.peer_connection.close()
