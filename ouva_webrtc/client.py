"""
CUstom async socketio client
"""
import socketio
from socketio import exceptions
from socketio import packet


class CustomAsyncClient(socketio.AsyncClient):
	"""
	CustomAsyncClient because the existing clients lack a method to emit multiple events.
	This doesn't conform to the standard socketio client, but the client implemented this way.
	"""

	async def emit_multiple(self, event, namespace=None, callback=None):
		"""
		Emit a custom event to one or more connected clients.

		This method is required because socketio emit implementation converts the event to a list,
		but the client does not want this for some cases.

		:param event: The event name. It can be any string. The event names
		              ``'connect'``, ``'message'`` and ``'disconnect'`` are
		              reserved and should not be used.
		:param namespace: The Socket.IO namespace for the event. If this
		                  argument is omitted the event is emitted to the
		                  default namespace.
		:param callback: If given, this function will be called to acknowledge
		                 the the server has received the message. The arguments
		                 that will be passed to the function are those provided
		                 by the server.

		Note: this method is not designed to be used concurrently. If multiple
		tasks are emitting at the same time on the same client connection, then
		messages composed of multiple packets may end up being sent in an
		incorrect sequence. Use standard concurrency solutions (such as a Lock
		object) to prevent this situation.

		Note 2: this method is a coroutine.
		"""
		namespace = namespace or '/'
		if namespace not in self.namespaces:
			raise exceptions.BadNamespaceError(
				namespace + ' is not a connected namespace.')
		self.logger.debug('Emitting event "%s" [%s]', event, namespace)
		if callback is not None:
			_id = self._generate_ack_id(namespace, callback)
		else:
			_id = None
		await self._send_packet(self.packet_class(
			packet.EVENT, namespace=namespace, data=event, id=_id))
