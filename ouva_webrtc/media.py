"""
This file contains all media sinks
"""
import logging

from aiortc import MediaStreamTrack
from aiortc.contrib.media import MediaRecorderContext, MediaRecorder
from aiortc.mediastreams import MediaStreamError
from av import VideoFrame


class ResizedMediaRecorder(MediaRecorder):
	"""
	A media sink that writes audio and/or resized video to a file.

	Examples:

	.. code-block:: python

		# Write to a video file.
		player = MediaRecorder('/path/to/file.mp4')

		# Write to a set of images.
		player = MediaRecorder('/path/to/file-%3d.png')

	:param file: The path to a file, or a file-like object.
	:param format: The format to use, defaults to autodect.
	:param options: Additional options to pass to FFmpeg.
	"""

	def __init__(self, file, format=None, options={}, width=None, height=None):
		super().__init__(file, format, options)

		self.width = width
		self.height = height
		self._index = 0

	async def __run_track(self, track: MediaStreamTrack, context: MediaRecorderContext):
		while True:
			try:
				frame = await track.recv()
			except MediaStreamError:
				return

			self._index += 1
			if self._index == 1:
				logging.info("streaming is started")
			if self._index % 100 == 0:
				logging.info("frame number: %d", self._index)

			if not context.started:
				# adjust the output size to match the first frame
				if isinstance(frame, VideoFrame):
					if self.width is None:
						context.stream.width = frame.width
						context.stream.height = frame.height
					else:
						context.stream.width = self.width
						context.stream.height = self.height
				context.started = True

			for packet in context.stream.encode(frame):
				self.__container.mux(packet)
