"""
This is a client for a WebRTC server.
It connects to the server and streams video from the camera to the server and records the video.
"""
import argparse
import asyncio
import logging.config
import multiprocessing as mp
import signal
import sys
from urllib import parse

import uvloop
import yaml

from ouva_webrtc import SIO
from ouva_webrtc.namespace import WebRTCCameraNamespace

with open("logging_config.yaml", 'r') as f:
	config = yaml.safe_load(f.read())

logging.config.dictConfig(config)

logger = logging.getLogger("ouva")


async def main(uri: str, query: str, namespace: WebRTCCameraNamespace):
	"""
	main async function to run the WebRTC client
	:param uri: webrtc uri
	:param query: query string
	:param namespace: WebRTCCameraNamespace instance
	"""
	try:
		SIO.register_namespace(namespace)
		await SIO.connect(
			uri + "/?" + query,
			transports=["websocket"])
		sid = SIO.sid
		ping_interval = SIO.eio.ping_interval
		logger.info("connection is established. sid: %s, ping interval: %d", sid, ping_interval)
	except Exception as ex:
		logger.info(ex)

	logger.info("waiting SIO")
	await SIO.wait()
	await namespace.stop()
	logger.info("wait ended")
	logger.info("shutdown ended")


async def shutdown(signal, loop, namespace):
	"""Cleanup tasks tied to the service's shutdown."""
	logger.info("Received exit signal %s", signal.name)
	await namespace.stop()
	await SIO.disconnect()
	logger.debug("namespace is stopped")
	tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]

	[task.cancel() for task in tasks]
	logger.debug("tasks are cancelled")

	await asyncio.gather(*tasks, return_exceptions=True)
	logger.debug("tasks are gathered")

	loop.stop()
	logger.debug("shutdown complete.")


class WebRTC(mp.Process):
	"""
	WebRTC client process
	Forks a new process to run the WebRTC client.
	Doesn't look like it's necessary to run in a separate process, but it's done for consistency.
	"""

	def __init__(self, uri: str, width: int, height: int):
		super().__init__()

		self.session_reference_key = "webrtc"

		parsed_url = parse.urlsplit(uri)

		stream_uri = f"wss://{parsed_url.hostname}:{parsed_url.port}"

		query = parsed_url.query

		params = parse.parse_qs(query)
		if params.get("camera-name") is None:
			self._fail("camera id is missing from the URI", exitcode=-1)
			return
		if len(params.get("camera-name")) != 1:
			self._fail("camera id is missing from the URI", exitcode=-1)
			return
		camera_id = params['camera-name'][0]

		auth_token = ""  # Optional
		params['auth'] = [parse.quote(auth_token)]
		query = "&".join([f"{key}={value[0]}" for key, value in params.items()])

		self.stream_uri: str = stream_uri
		self.camera_id: str = camera_id
		self.query: str = query

		self.width = width
		self.height = height

		self.daemon = True
		self._exitcode = 0
		self.entry_task = None

	def _fail(self, error_message: str = "", exitcode: int = 2) -> None:
		self._exitcode = exitcode
		logger.error("session %s: %s", self.session_reference_key, error_message)

	def run(self) -> None:
		"""
		Start the process.
		:return:
		"""
		uvloop.install()

		loop = asyncio.get_event_loop()
		signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)
		namespace = WebRTCCameraNamespace(
			"/",
			camera_id=self.camera_id,
			width=self.height, height=self.width
		)
		for s in signals:
			loop.add_signal_handler(
				s, lambda s=s: asyncio.create_task(shutdown(s, loop, namespace)))
		try:
			entry_task = loop.create_task(main(self.stream_uri, self.query, namespace))
			loop.run_until_complete(entry_task)
		except (KeyboardInterrupt, RuntimeError, asyncio.exceptions.CancelledError):
			pass
		finally:
			if loop.is_running():
				loop.run_until_complete(shutdown(signal.SIGTERM, loop, namespace))
		logger.info("exiting...")
		sys.exit(0)

	def stop(self):
		"""
		Stop the process.
		"""
		if self.entry_task is not None:
			self.entry_task.cancel()


NUM_SIGNALS = 0


def exit_gracefully(signum, _):
	"""
	Exit gracefully on signal.
	:param signum: signal number
	:param _: frame
	"""
	global NUM_SIGNALS
	NUM_SIGNALS += 1
	logger.info("%s signal received, exiting...", signal.strsignal(signum))
	if NUM_SIGNALS > 1:
		logger.warning("force exit")
		webrtc.kill()
		webrtc.join()
		sys.exit(1)
	else:
		webrtc.stop()
		sys.exit(0)


signal.signal(signal.SIGINT, exit_gracefully)
signal.signal(signal.SIGTERM, exit_gracefully)

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--uri", type=str,
	                    default="webrtc://signalling.mydomain.com:3443?client-type=ouva&camera-name=camera_6TMjwrppQ-o&room-name=some_type&device-id=679&org-id=2")
	# most of these are the values requested by the client.
	# Connection will pass anything in the query string to the server. Only camera-name is required for this client.
	# You can set the other values to whatever you want to identify the peer.
	parser.add_argument("--width", type=int, default=1280)
	parser.add_argument("--height", type=int, default=720)

	args = parser.parse_args()

	webrtc = WebRTC(uri=args.uri, width=args.width, height=args.height)
	webrtc.start()

	webrtc.join()
	logger.info("joined to webrtc")
	sys.exit(0)
