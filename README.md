# Example WebRTC Client

This project is a WebRTC client implemented in Python. The client connects to a WebRTC server via a Socket.IO
connection. The client can be used to record the video stream from a WebRTC server

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing
purposes.

### Installing

Clone the repository to your local machine:

```bash
git clone https://gitlab.com/ouvaco/webrtc-integration-example.git
```

Navigate to the project directory:

```bash
cd webrtc-integration-example
```

Create and activate a virtual environment:

```bash
python -m venv venv
source venv/bin/activate
```

Install the required packages:

```bash
pip install -r requirements.txt
```

## Running the Application

You can run the application with the following command:

```bash
python webrtc_client.py --uri "webrtc://signalling.mydomain.com:3443?client-type=ouva&camera-name=camera_6TMjwrppQ-o&room-name=some_type&device-id=679&org-id=2" --width 1280 --height 720
```

Replace the `--uri`, `--width`, and `--height` parameters with your own values.